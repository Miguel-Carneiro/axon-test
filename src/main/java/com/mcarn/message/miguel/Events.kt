package com.mcarn.message.miguel

interface MiguelEvent {
    val id: String
}

data class MiguelInsertedEvent(override val id: String) : MiguelEvent
data class MiguelUpdatedEvent(override val id: String) : MiguelEvent
data class MiguelDeletedEvent(override val id: String) : MiguelEvent