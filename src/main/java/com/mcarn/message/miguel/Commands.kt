package com.mcarn.message.miguel

import org.axonframework.modelling.command.TargetAggregateIdentifier

interface MiguelCommand {
    val id: String
}

data class UpdateMiguelCommand(@TargetAggregateIdentifier override val id: String) : MiguelCommand

data class DeleteMiguelCommand(@TargetAggregateIdentifier override val id: String) : MiguelCommand

