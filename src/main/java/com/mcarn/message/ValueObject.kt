package com.mcarn.message

data class SequencePair(
        val id: Int,
        val text: String
)

data class SequencePairs(var pairs: List<SequencePair>) {
    fun addPair(pair: SequencePair) {
        this.pairs += pair
    }
}