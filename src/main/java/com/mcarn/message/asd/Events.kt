package com.mcarn.message.asd

interface AsdEvent {
    val id: String
}

data class AsdInsertedEvent(override val id: String) : AsdEvent
data class AsdUpdatedEvent(override val id: String) : AsdEvent
data class AsdDeletedEvent(override val id: String) : AsdEvent