package com.mcarn.message.asd

import org.axonframework.modelling.command.TargetAggregateIdentifier

interface AsdCommand {
    val id: String
}

data class UpdateAsdCommand(@TargetAggregateIdentifier override val id: String) : AsdCommand

data class DeleteAsdCommand(@TargetAggregateIdentifier override val id: String) : AsdCommand

data class UpdateAsdExtCommand(@TargetAggregateIdentifier override val id: String) : AsdCommand