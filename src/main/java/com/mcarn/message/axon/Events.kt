package com.mcarn.message.axon

interface AxonEvent {
    val id: String
}

data class AxonInsertedEvent(override val id: String) : AxonEvent
data class AxonUpdatedEvent(override val id: String) : AxonEvent
data class AxonDeletedEvent(override val id: String) : AxonEvent