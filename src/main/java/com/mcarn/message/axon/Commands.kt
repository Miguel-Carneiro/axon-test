package com.mcarn.message.axon

import org.axonframework.modelling.command.TargetAggregateIdentifier

interface AxonCommand {
    val id: String
}

data class UpdateAxonCommand(@TargetAggregateIdentifier override val id: String) : AxonCommand

data class DeleteAxonCommand(@TargetAggregateIdentifier override val id: String) : AxonCommand

data class UpdateAxonExtCommand(@TargetAggregateIdentifier override val id: String) : AxonCommand