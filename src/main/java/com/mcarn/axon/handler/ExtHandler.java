package com.mcarn.axon.handler;

import com.mcarn.message.SequencePairs;
import com.mcarn.message.axon.UpdateAxonCommand;
import com.mcarn.message.axon.UpdateAxonExtCommand;
import java.util.concurrent.ExecutionException;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.commandhandling.GenericCommandMessage;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExtHandler {
  private final CommandBus commandBus;
  private final CommandGateway commandGateway;

  @Autowired
  public ExtHandler(CommandBus commandBus,
                    CommandGateway commandGateway) {
    this.commandBus = commandBus;
    this.commandGateway = commandGateway;
  }


  @CommandHandler
  public synchronized SequencePairs handle(UpdateAxonExtCommand cmd)
      throws ExecutionException, InterruptedException {

    var vaa = commandGateway.<SequencePairs>send(GenericCommandMessage.asCommandMessage(
        new UpdateAxonCommand(cmd.getId())
    ));

    System.out.println("out " + vaa);
    System.out.println("out1 " + vaa.get());
    return vaa.get();
  }
}
