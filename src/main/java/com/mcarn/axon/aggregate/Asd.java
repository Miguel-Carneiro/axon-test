package com.mcarn.axon.aggregate;

import static java.lang.Math.toIntExact;
import static org.axonframework.modelling.command.AggregateLifecycle.apply;
import static org.axonframework.modelling.command.AggregateLifecycle.markDeleted;

import com.mcarn.message.SequencePair;
import com.mcarn.message.SequencePairs;
import com.mcarn.message.asd.AsdDeletedEvent;
import com.mcarn.message.asd.AsdInsertedEvent;
import com.mcarn.message.asd.AsdUpdatedEvent;
import com.mcarn.message.asd.DeleteAsdCommand;
import com.mcarn.message.asd.UpdateAsdCommand;
import java.util.ArrayList;
import java.util.List;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventhandling.SequenceNumber;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.CreationPolicy;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class Asd {

  @AggregateIdentifier
  private String id;
  private Long sequence;

  public Asd() {
    // Required by Axon
  }

  @CommandHandler
  @CreationPolicy(AggregateCreationPolicy.CREATE_IF_MISSING)
  public SequencePairs handle(UpdateAsdCommand cmd) {
    List<SequencePair> pairList = new ArrayList<>();
    if (null == id) {
      apply(new AsdInsertedEvent(cmd.getId()));
      var pair = new SequencePair(toIntExact(sequence), cmd.getClass().getName());
      pairList.add(pair);
    }

    apply(new AsdUpdatedEvent(cmd.getId()));

    var pair = new SequencePair(toIntExact(sequence), cmd.getClass().getName());
    pairList.add(pair);
    return new SequencePairs(pairList);
  }

  @CommandHandler
  public SequencePairs handle(DeleteAsdCommand cmd) {
    List<SequencePair> pairList = new ArrayList<>();


    apply(new AsdDeletedEvent(cmd.getId()));

    var pair = new SequencePair(toIntExact(sequence), cmd.getClass().getName());
    pairList.add(pair);

    return new SequencePairs(pairList);
  }

  @EventSourcingHandler
  public void on(AsdInsertedEvent event, @SequenceNumber Long sequenceNumber) {
    this.id = event.getId();
    this.sequence = sequenceNumber;
  }

  @EventSourcingHandler
  public void on(AsdUpdatedEvent event, @SequenceNumber Long sequenceNumber) {
    this.sequence = sequenceNumber;
  }

  @EventSourcingHandler
  public void on(AsdDeletedEvent event, @SequenceNumber Long sequenceNumber) {
    this.sequence = sequenceNumber;
    markDeleted();
  }
}
