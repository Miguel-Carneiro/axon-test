package com.mcarn.axon.aggregate;

import static org.axonframework.modelling.command.AggregateLifecycle.apply;

import com.mcarn.message.SequencePair;
import com.mcarn.message.SequencePairs;
import com.mcarn.message.axon.AxonDeletedEvent;
import com.mcarn.message.axon.AxonInsertedEvent;
import com.mcarn.message.axon.AxonUpdatedEvent;
import com.mcarn.message.axon.DeleteAxonCommand;
import com.mcarn.message.axon.UpdateAxonCommand;
import java.util.ArrayList;
import java.util.List;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventhandling.SequenceNumber;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.CreationPolicy;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class Axon {
  @AggregateIdentifier
  private String id;
  private Long value;

  @CommandHandler
  @CreationPolicy(AggregateCreationPolicy.CREATE_IF_MISSING)
  public SequencePairs handle(UpdateAxonCommand cmd) {
    List<SequencePair> pairList = new ArrayList<>();
    if (null == id) {
      apply(new AxonInsertedEvent(cmd.getId()));
      var pair = new SequencePair(Math.toIntExact(value), cmd.getClass().getName());
      pairList.add(pair);
    }
    apply(new AxonUpdatedEvent(cmd.getId()));
    var pair = new SequencePair(Math.toIntExact(value), cmd.getClass().getName());
    pairList.add(pair);
    return new SequencePairs(pairList);
  }

  @CommandHandler
  public SequencePairs handle(DeleteAxonCommand cmd) {
    List<SequencePair> pairList = new ArrayList<>();
    new AxonDeletedEvent(cmd.getId());
    var pair = new SequencePair(Math.toIntExact(value), cmd.getClass().getName());
    pairList.add(pair);
    return new SequencePairs(pairList);
  }

  @EventSourcingHandler
  public void on(AxonInsertedEvent evnt, @SequenceNumber Long sequenceNumber) {
    id = evnt.getId();
    value = sequenceNumber;
  }


  @EventSourcingHandler
  public void on(AxonUpdatedEvent evnt, @SequenceNumber Long sequenceNumber) {
    value = sequenceNumber;
  }

  @EventSourcingHandler
  public void on(AxonDeletedEvent evnt, @SequenceNumber Long sequenceNumber) {
    value = sequenceNumber;
  }


}
