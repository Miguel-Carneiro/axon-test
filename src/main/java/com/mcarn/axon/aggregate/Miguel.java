package com.mcarn.axon.aggregate;

import static java.lang.Math.toIntExact;
import static org.axonframework.modelling.command.AggregateLifecycle.apply;

import com.mcarn.message.SequencePair;
import com.mcarn.message.SequencePairs;
import com.mcarn.message.miguel.DeleteMiguelCommand;
import com.mcarn.message.miguel.MiguelDeletedEvent;
import com.mcarn.message.miguel.MiguelInsertedEvent;
import com.mcarn.message.miguel.MiguelUpdatedEvent;
import com.mcarn.message.miguel.UpdateMiguelCommand;
import java.util.ArrayList;
import java.util.List;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventhandling.SequenceNumber;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateCreationPolicy;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.CreationPolicy;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class Miguel {

  @AggregateIdentifier
  private String id;
  private Long sequence;
  private boolean isDisabled;

  public Miguel() {
    // Axon
  }

  @CommandHandler
  @CreationPolicy(AggregateCreationPolicy.CREATE_IF_MISSING)
  public SequencePairs handle(UpdateMiguelCommand cmd) {
    List<SequencePair> pairList = new ArrayList<>();
    if (null == this.id || isDisabled) {
      apply(new MiguelInsertedEvent(cmd.getId()));
      var pair = new SequencePair(toIntExact(sequence), cmd.getClass().getName());
      pairList.add(pair);
    }

    apply(new MiguelUpdatedEvent(cmd.getId()));

    var pair = new SequencePair(toIntExact(sequence), cmd.getClass().getName());
    pairList.add(pair);
    return new SequencePairs(pairList);
  }

  @CommandHandler
  public SequencePairs handle(DeleteMiguelCommand cmd) {
    List<SequencePair> pairList = new ArrayList<>();

    if (null != this.id && !isDisabled) {
      apply(new MiguelDeletedEvent(cmd.getId()));

      var pair = new SequencePair(toIntExact(sequence), cmd.getClass().getName());
      pairList.add(pair);
    }

    return new SequencePairs(pairList);

  }


  @EventSourcingHandler
  public void on(MiguelInsertedEvent event, @SequenceNumber Long sequenceNumber) {
    this.id = event.getId();
    this.sequence = sequenceNumber;
    this.isDisabled = false;
  }

  @EventSourcingHandler
  public void on(MiguelUpdatedEvent event, @SequenceNumber Long sequenceNumber) {
    this.sequence = sequenceNumber;
  }

  @EventSourcingHandler
  public void on(MiguelDeletedEvent event, @SequenceNumber Long sequenceNumber) {
    this.sequence = sequenceNumber;
    this.isDisabled = true;
    // markDeleted();
  }
}
