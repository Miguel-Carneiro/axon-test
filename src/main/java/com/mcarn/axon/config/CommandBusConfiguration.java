package com.mcarn.axon.config;

import io.opentracing.Tracer;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.extensions.tracing.MessageTagBuilderService;
import org.axonframework.extensions.tracing.OpenTraceDispatchInterceptor;
import org.axonframework.extensions.tracing.OpenTraceHandlerInterceptor;
import org.axonframework.extensions.tracing.TracingCommandGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CommandBusConfiguration {

  /**
   * Configure the command bus by registering the message handler interceptors for the Version
   * aggregate authorization and logging.
   */
  @Autowired
  public void commandBus(CommandBus commandBus,
                         OpenTraceHandlerInterceptor openTraceHandlerInterceptor) {

    /*
     OpenTraceHandlerInterceptor registered first so that it activates a Tracing Span that will
     be used by the InitTagsAndCommandLoggingInterceptor
    */
    commandBus.registerHandlerInterceptor(openTraceHandlerInterceptor);
  }

  /**
   * Overriding the CommandGateway configured by axon-tracing-spring-boot-starter
   * because we want to register the OpenTraceHandlerInterceptor manually
   * as the first interceptor in the chain. See method above.
   */
  @Bean
  public CommandGateway tracingCommandGateway(Tracer tracer,
                                              CommandBus commandBus,
                                              OpenTraceDispatchInterceptor openTraceDispatchInterceptor,
                                              OpenTraceHandlerInterceptor openTraceHandlerInterceptor,
                                              MessageTagBuilderService messageTagBuilderService) {

    commandBus.registerHandlerInterceptor(openTraceHandlerInterceptor);

    TracingCommandGateway tracingCommandGateway =
        TracingCommandGateway.builder()
            .tracer(tracer)
            .delegateCommandBus(commandBus)
            .messageTagBuilderService(messageTagBuilderService)
            .build();
    tracingCommandGateway.registerDispatchInterceptor(openTraceDispatchInterceptor);
    return tracingCommandGateway;
  }
}
